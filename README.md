## 项目说明

项目是基于Laravel作为后端服务，uniapp作为前端服务。管理端使用的是Laravel-admin开发。

## 环境要求

1. MySQL的版本必须>=5.7。
2. PHP的版本必须是7.4。要求必须安装Redis、fileinfo扩展。
3. Redis的版本没有特别的要求，强烈建议在7.0左右的版本，因为开发环境的版本就是7.0。
4. NGINX作为web服务，没有特别的要求。
5. 所有的图片都采用七牛云存储，后续会支持更多的存储方式，暂且只支持该方式。

## 部署流程

1. 下面是一个简易的说明。很多人是比较喜欢宝塔部署项目。在添加域名的时候，将项目的根目录指向public目录。
2. 配置好域名之后，一般会在public目录下生成一个.user.ini的文件，记得把这个文件的内容使用#给注释掉。
3. 对于storage要进行读写权限。给775权限。
4. 对于数据库配置，将项目中的rabbit_examp.sql文件导入到数据库中。然后去修改.env(将.env.example复制一份重命名为.env)文件的配置信息。
6. 部署要之后用你的域名+`rabbit/exam`访问管理端。例如http://baidu.com/rabbit/exam。

## 官方客服

欢迎你在使用兔兔答题的过程中，遇到任何问题都可以添加官方客服和官方技术进行支持，也欢迎你一起参与交流。

![](./image/guanfangkefu.jpeg)

## 仓库地址

1、[后端开源地址](https://gitee.com/shyjfang_admin/tutudati_template01_api)

2、[前端开源地址](https://gitee.com/shyjfang_admin/tutudati_template01_user)

3、[文档地址](https://wiki.tutudati.com/doc/56/)

## 预览图

![](https://imgcdn.tutudati.com/tutudatitemplate001.jpeg)

![](https://imgcdn.tutudati.com/tutudatitemplate002.jpeg)

![](https://imgcdn.tutudati.com/tutudatitemplate003.jpeg)

![](https://imgcdn.tutudati.com/tutudatitemplate004.jpeg)
