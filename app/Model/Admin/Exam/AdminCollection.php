<?php
declare(strict_types=1);

namespace App\Model\Admin\Exam;

use App\Model\Common\Exam\Collection;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/17
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class AdminCollection extends Collection
{
    protected $appends = [
        "cover"
    ];

    public function getCoverAttribute($key): string
    {
        return $this->getAttribute("url") . $this->getAttribute("path");
    }

    public function setPictureAttribute($pictures)
    {
        if (is_array($pictures)) {
            $this->attributes['picture'] = json_encode($pictures);
        }
    }

    public static function getList(): array
    {
        $items = self::query()->orderByDesc("id")->get(["uid", "title"]);
        $list = [];
        foreach ($items as $value) {
            $list[$value->uid] = $value->title;
        }

        return $list;
    }
}
