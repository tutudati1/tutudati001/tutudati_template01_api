<?php
declare(strict_types = 1);

namespace App\Model\Admin\Member;

use App\Model\Common\Member\Config;

/**
 * 会员等级配置
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/17
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class AdminConfig extends Config
{

    protected $appends = [
        "money_unit"
    ];

    public function getMoneyUnitAttribute($key): string
    {
        return $this->getAttribute("money") . "/" . $this->getAttribute("unit");
    }

    public static function getList(): array
    {
        $items = self::query()->where("is_show", "=", 1)->get(["uid", "title"]);
        $list = [];
        foreach ($items as $value) {
            $list[$value->uid] = $value->title;
        }
        return $list;
    }
}