<?php
declare(strict_types=1);

namespace App\Model\Admin\Resource;

use App\Model\Common\Resource\Resource;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/5/16
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class AdminResource extends Resource
{
    protected $appends = [
        "cover"
    ];

    public function getCoverAttribute($key): string
    {
        return $this->getAttribute("url") . $this->getAttribute("path");
    }
}
