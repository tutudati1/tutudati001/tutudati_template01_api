<?php
declare(strict_types=1);

namespace App\Model\Admin\Config;

use App\Model\Common\Config\FriendlyLink;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/9
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class AdminFriendlyLink extends FriendlyLink
{
    protected $appends = [
        "cover"
    ];

    public function getCoverAttribute($key): string
    {
        if (empty($this->getAttribute("path"))) {
            return "";
        }
        return $this->getAttribute("url") . $this->getAttribute("path");
    }
}
