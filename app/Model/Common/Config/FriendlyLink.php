<?php
declare(strict_types = 1);
namespace App\Model\Common\Config;

use App\Model\Common\BaseModel;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/9
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class FriendlyLink extends BaseModel
{
    protected $table = "friendly_link";

    protected $fillable = [
        "uid",
        "title",
        "url",
        "path",
        "is_show",
        "sort",
        "remark",
        "site_url",
    ];
}
