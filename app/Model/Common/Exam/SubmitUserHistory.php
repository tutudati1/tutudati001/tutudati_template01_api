<?php
declare(strict_types=1);

namespace App\Model\Common\Exam;

use App\Model\Common\BaseModel;
use App\Model\Common\User\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * 用户答题提交记录
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/22
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class SubmitUserHistory extends BaseModel
{
    protected $table = "ex_collection_user_history";

    protected $fillable = [
        "uid",
        "collection_uid",
        "user_uid",
        "is_show",
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, "user_uid", "uid");
    }
}
