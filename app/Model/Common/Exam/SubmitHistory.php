<?php
declare(strict_types=1);

namespace App\Model\Common\Exam;

use App\Model\Common\BaseModel;

/**
 * 答题汇总记录
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/8/4
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class SubmitHistory extends BaseModel
{
    protected $table = "ex_exam_submit_history";

    protected $fillable = [
        "uid",
        "ex_exam_submit",
        "user_uid",
        "collection_uid",
        "exam_uid",
        "type",
        "score",
        "answer",
        "sort",
        "is_show",
        "origin_score",
        "origin_answer",
    ];
}
