<?php
declare(strict_types=1);

namespace App\Model\Common\Exam;

use App\Model\Common\BaseModel;

/**
 * 答题汇总记录
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/20
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class Submit extends BaseModel
{
    protected $table = "ex_exam_submit";

    protected $fillable = [
        "uid",
        "user_uid",
        "collection_uid",
        "score",
        "is_show",
        "exam_count",
        "correct_count",
        "error_count",
        "submit_time",
    ];

    protected $casts = [
        "uid" => "string",
        "collection_uid" => "string",
    ];
}
