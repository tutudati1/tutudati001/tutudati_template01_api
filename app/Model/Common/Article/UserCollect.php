<?php

declare(strict_types=1);

namespace App\Model\Common\Article;

use App\Model\Common\BaseModel;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/24
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class UserCollect extends BaseModel
{   
    protected $table = "article_collect";

    protected $fillable = [
        "user_uid",
        "article_uid",
        "is_show",
        "uid",
    ];
}
