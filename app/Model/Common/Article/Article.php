<?php
declare(strict_types=1);

namespace App\Model\Common\Article;

use App\Model\Common\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/23
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class Article extends BaseModel
{
    protected $table = "article";

    protected $fillable = [
        "title",
        "is_show",
        "sort",
        "uid",
        "content",
        "cate_uid",
        "url",
        "path",
        "read_count",
        "collect_count",
        "remark",
        "keywords",
        "office_url",
        "author",
    ];

    protected $casts = [
        "uid" => "string",
        "cate_uid" => "string",
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, "cate_uid", "uid");
    }

    public function getKeywordsAttribute($key): array
    {
        return !empty($key) ? explode(",", $key) : [];
    }

    public function getPathAttribute($key): string
    {
        return $key . "?imageView2/2/format/webp/q/95!";
    }
}
