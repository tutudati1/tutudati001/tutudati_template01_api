<?php
declare(strict_types=1);

namespace App\Model\Common\Article;

use App\Model\Common\BaseModel;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/23
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class Category extends BaseModel
{
    protected $table = "article_cate";

    protected $fillable = [
        "title",
        "is_show",
        "sort",
        "uid",
        "seo_title",
        "seo_keywords",
        "seo_description"
    ];
}
