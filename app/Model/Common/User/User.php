<?php
declare(strict_types=1);
/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/9
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */

namespace App\Model\Common\User;

use App\Model\Common\BaseModel;
use App\Model\Common\Config\Profession;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class User extends BaseModel
{
    protected $table = "user";

    protected $fillable = [
        "uid",
        "openid",
        "nickname",
        "mobile",
        "email",
        "avatar_url",
        "gender",
        "age",
        "birthday",
        "remark",
        "profession_uid",
        "score",
        "name",
        "is_update_info",
        "is_vip",
        "vip_starttime",
        "vip_endtime",
        "code",
        "account",
        "password"
    ];

    protected $casts = [
        "profession_uid" => "string",
        "uid" => "string",
    ];

    public function profession(): BelongsTo
    {
        return $this->belongsTo(Profession::class, "profession_uid", "uid")
            ->select(["uid", "title"]);
    }
}
