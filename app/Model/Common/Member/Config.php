<?php
declare(strict_types=1);

namespace App\Model\Common\Member;

use App\Model\Common\BaseModel;

/**
 * 会员等级配置
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/17
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class Config extends BaseModel
{
    protected $table = "user_vip_config";

    protected $fillable = [
        "uid",
        "is_show",
        "sort",
        "money",
        "unit",
        "title",
        "remark",
        "equity",
    ];
}
