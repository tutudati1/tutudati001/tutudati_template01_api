<?php
declare(strict_types=1);

namespace App\Model\Common\Document;

use App\Model\Common\BaseModel;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/23
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class DocumentGroup extends BaseModel
{
    protected $table = "document_group";

    protected $fillable = [
        "uid",
        "title",
        "is_show",
        "sort"
    ];

    public function document(): HasMany
    {
        return $this->hasMany(Document::class, "group_uid", "uid")
            ->where("is_show", "=", 1);
    }
}
