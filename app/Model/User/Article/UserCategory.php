<?php
declare(strict_types=1);

namespace App\Model\User\Article;

use App\Model\Common\Article\Category;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/23
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class UserCategory extends Category
{

}
