<?php
declare(strict_types=1);

namespace App\Model\User\User;

use App\Model\Common\User\User;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/9
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class UserModel extends User
{

}
