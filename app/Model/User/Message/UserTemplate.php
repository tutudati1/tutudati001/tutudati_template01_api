<?php

declare(strict_types=1);

namespace App\Model\User\Message;

use App\Model\Common\Message\Template;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/5/25
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class UserTemplate extends Template
{
}
