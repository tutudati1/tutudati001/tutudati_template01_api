<?php
declare(strict_types=1);

namespace App\Model\User\Exam;

use App\Model\Common\Exam\CollectionHistory;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/22
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class UserCollectionCollectionHistory extends CollectionHistory
{

}
