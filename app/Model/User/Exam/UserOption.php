<?php

declare(strict_types=1);

namespace App\Model\User\Exam;

use App\Model\Common\Exam\Option;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/24
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class UserOption extends Option
{

}