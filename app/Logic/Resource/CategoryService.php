<?php
declare(strict_types=1);

namespace App\Logic\Resource;

use App\Logic\BaseUserService;
use App\Model\User\Resource\UserCategory;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/5/25
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class CategoryService extends BaseUserService
{
    public function getList(): array
    {
        return UserCategory::query()
            ->with(["children:uid,title,path,url,parent_uid"])
            ->where([
                ["is_show", "=", 1],
                ["parent_uid", "=", 0]
            ])->orderByDesc("sort")
            ->get(["uid", "title"])
            ->toArray();
    }
}
