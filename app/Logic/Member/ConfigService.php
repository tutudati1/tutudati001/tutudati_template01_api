<?php
declare(strict_types=1);

namespace App\Logic\Member;

use App\Logic\BaseUserService;
use App\Model\User\Member\UserConfig;

/**
 * 会员等级配置
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/17
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class ConfigService extends BaseUserService
{
    // 会员权益列表
    public function getList(): array
    {
        return UserConfig::query()
            ->where([
                ["is_show", "=", 1]
            ])->orderByDesc("sort")
            ->get(["title", "money", "unit", "remark", "uid"])
            ->toArray();
    }

    // 会员权益内容
    public function getContent(): array
    {
        $requestParams = request()->all();
        if (!empty($requestParams["uid"])) {
            $bean = UserConfig::query()->where([
                ["is_show", "=", 1],
                ["uid", "=", $requestParams["uid"]]
            ])->first(["equity"]);
            if (!empty($bean)) return $bean->toArray();
        }
        return [];
    }
}
