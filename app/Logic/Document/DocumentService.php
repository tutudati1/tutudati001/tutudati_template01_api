<?php
declare(strict_types=1);

namespace App\Logic\Document;

use App\Logic\BaseUserService;
use App\Model\User\Document\UserDocument;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/19
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class DocumentService extends BaseUserService
{
    public function getContent(): array
    {
        $requestParams = request()->all();
        if (!empty($requestParams["uid"])) {
            $bean = UserDocument::query()
                ->where([
                    ["is_show", "=", 1],
                    ["uid", "=", $requestParams["uid"]]
                ])
                ->first(["title", "uid", "content"]);
            return !empty($bean) ? $bean->toArray() : [];
        }
        return [];
    }
}
