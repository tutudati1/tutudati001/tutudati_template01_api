<?php

declare(strict_types=1);

namespace App\Logic\User;

use App\Logic\BaseUserService;
use App\Model\User\User\UserModel;
use Illuminate\Support\Facades\Cache;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/19
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class UserService extends BaseUserService
{
    public function updateUserInfo(): array
    {
        $requestParams = request()->all();
        $updateUserInfo = [
            "nickname" => $requestParams["nickname"],
            "avatar_url" => $requestParams["avatar_url"],
            //"profession_uid" => $requestParams["profession_uid"],
            "gender" => $requestParams["gender"],
            "remark" => $requestParams["remark"],
            //"name" => $requestParams["name"],
        ];
//        if (!empty($requestParams["name"])) {
//            $updateUserInfo["name"] = $requestParams["name"];
//        }
//        if (!empty($requestParams["birthday"])) {
//            $updateUserInfo["birthday"] = $requestParams["birthday"];
//        }
        if (!empty($requestParams["remark"])) {
            $updateUserInfo["remark"] = $requestParams["remark"];
        }
        if (!empty($requestParams["password"])) {
            $updateUserInfo["password"] = md5(trim($requestParams["password"]));
        }
        $updateRow = UserModel::query()
            ->where("uid", "=", $this->getUserUid())
            ->update($updateUserInfo);
        if ($updateRow) {
            $userCacheInfo = $this->userInfo();
            $userCacheInfo["nickname"] = $requestParams["nickname"];
            $userCacheInfo["name"] = $requestParams["name"] ?? "";
            $userCacheInfo["avatar_url"] = $requestParams["avatar_url"];
            // $userCacheInfo["profession_uid"] = $requestParams["profession_uid"];
            $userCacheInfo["gender"] = $requestParams["gender"];
            $userCacheInfo["remark"] = $requestParams["remark"] ?? "";
            $userCacheInfo["birthday"] = $requestParams["birthday"] ?? "";
            Cache::put("login:" . $userCacheInfo["login_token"], $userCacheInfo);
            return ["errcode" => 0, "errmsg" => "信息更新成功", "data" => $userCacheInfo];
        }
        return ["errcode" => 1, "errmsg" => "信息更新失败"];
    }

    public function getUserAvatarInfo(): array
    {
        $userInfo = $this->userInfo();
        return [
            "avatar_url" => $userInfo["avatar_url"] ?? "https://img1.imgtp.com/2023/08/23/WGGzh8E7.png",
            "nickname" => $userInfo["nickname"] ?? "请先登录",
            "code" => $userInfo["code"] ?? "",
        ];
    }

    public function getUserBasicInfo(): array
    {
        $userInfo = $this->userInfo();
        return [
            "avatar_url" => $userInfo["avatar_url"],
            "nickname" => $userInfo["nickname"],
            "profession" => $userInfo["profession"],
            "name" => $userInfo["name"] ?? "",
            "birthday" => $userInfo["birthday"] ?? "",
            "gender" => $userInfo["gender"] ?? 0,
            "remark" => $userInfo["remark"] ?? "",
            "profession_uid" => $userInfo["profession"]["uid"],
        ];
    }

    /**
     * 检查用户登录状态
     *
     * @return array
     */
    public function checkoutUserLoginState(): array
    {
        return $this->userInfo();
    }

    /**
     * 账号登录
     * @return array
     */
    public function accountLogin(): array
    {
        $params = request()->all();
        $userInfo = UserModel::query()->where("account", "=", $params["account"])->first(["account", "password"]);
        if (!empty($userInfo)) {
            $userInfo = $userInfo->toArray();
            if ($userInfo["password"] == md5($params["password"])) {
                return ["code" => 0, "msg" => "登录成功"];
            }
            return ["code" => -1, "msg" => "密码错误"];
        }
        return ["code" => -1, "msg" => "请先注册"];
    }

    /**
     * 账号注册
     * @return array
     */
    public function accountRegister(): array
    {
        $params = request()->all();
        $userInfo = UserModel::query()->where("account", "=", $params["account"])->first(["account"]);
        if (empty($userInfo)) {
            $row = UserModel::query()->where("uid", "=", $this->getUserUid())->update([
                "account" => $params["account"],
                "password" => md5($params["password"]),
            ]);
            if ($row) {
                return ["code" => 0, "登录成功", "msg" => "注册成功"];
            }
            return ["code" => -1, "msg" => "登录失败"];
        }
        return ["code" => -1, "msg" => "账号已存在"];
    }
}
