<?php
declare(strict_types=1);

namespace App\Logic\Web\Article;

use App\Model\User\Article\UserArticle;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/27
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class ArticleService
{
    /**
     * 获取首页文章列表
     * @return array
     */
    public function getHomeList(): array
    {
        return UserArticle::query()
            ->with(["category:uid,title"])
            ->where([
                ["is_show", "=", 1]
            ])
            ->orderByDesc("id")
            ->select(["uid", "title", "read_count", "collect_count", "remark", "cate_uid", "url", "path", "keywords",
                "author", "created_at"])
            ->limit(12)
            ->get()
            ->toArray();
    }

    // 获取首页热门文章
    public function getHomeHotList(): array
    {
        return UserArticle::query()
            ->where("is_show", "=", 1)
            ->orderByDesc("collect_count")
            ->select(["uid", "title", "url", "path"])
            ->limit(12)
            ->get()
            ->toArray();
    }

    // 文章列表页
    public function getList(array  $requestParams): LengthAwarePaginator
    {
        return UserArticle::query()
            ->with(["category:uid,title"])
            ->where([
                ["is_show", "=", 1]
            ])
            ->where(function ($query) use ($requestParams) {
                if (!empty($requestParams["cate_uid"])) {
                    $query->where("cate_uid", "=", $requestParams["cate_uid"]);
                }
                if (!empty($requestParams["title"])) {
                    $query->where("title", "like", "%{$requestParams['title']}%");
                }
            })
            ->orderByDesc("id")
            ->select(["uid", "title", "read_count", "collect_count", "remark", "cate_uid", "url", "path", "keywords",
                "author", "created_at"])
            ->paginate(8);
    }

    // 获取热门文章列表
    public function getHotList(): array
    {
        return UserArticle::query()
            ->where("is_show", "=", 1)
            ->orderByDesc("collect_count")
            ->select(["uid", "title", "url", "path"])
            ->limit(6)
            ->get()
            ->toArray();
    }

    // 文章详情
    public function getContent(array $requestParam): array
    {
        $bean = UserArticle::query()
            ->with(["category:uid,title"])
            ->where([
                ["uid", "=", $requestParam["uid"]],
                ["is_show", "=", 1]
            ])->select(["id", "uid", "title", "read_count", "collect_count", "remark", "cate_uid", "url", "path", "keywords",
                "author", "created_at", "content", "office_url"])
            ->first();
        // 查询上一篇和下一篇
        if (!empty($bean)) {
            // 增加文章阅读量
            $bean = $bean->toArray();
            $id = $bean["id"];
            UserArticle::query()->where("id", "=", $id)->increment("read_count");
            unset($bean["id"]);
            $next = UserArticle::query()->where([
                ["id", ">", $id],
                ["is_show", "=", 1]
            ])->first(["uid", "title", "path", "url"]);
            $pre = UserArticle::query()->where([
                ["id", "<", $id],
                ["is_show", "=", 1]
            ])->first(["uid", "title", "path", "url"]);
            if (!empty($next)) {
                $next = $next->toArray();
            }
            if (!empty($pre)) {
                $pre = $pre->toArray();
            }
            return [
                "content" => $bean,
                "next" => $next,
                "pre" => $pre
            ];
        }
        return [];
    }

    // 根据文章uid，查询出相关的文章
    public function getRelList(array $requestParam):array
    {
        $bean = UserArticle::query()->where([
            ["uid", "=", $requestParam["uid"]],
            ["is_show", "=", 1]
        ])->first(["cate_uid"]);
        if (!empty($bean)) {
            $cateUid = $bean->cate_uid;
           return UserArticle::query()->where([
                ["cate_uid", "=", $cateUid],
                ["is_show", "=", 1]
            ])->select(["title", "uid", "url", "path", "remark"])->limit(3)->get()->toArray();
        }
        return [];
    }
}
