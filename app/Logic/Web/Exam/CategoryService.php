<?php
declare(strict_types=1);

namespace App\Logic\Web\Exam;

use App\Model\User\Exam\UserCategory;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/27
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class CategoryService
{
    // 获取分类全量列表
    public function getList(): array
    {
        return UserCategory::query()->where("is_show", "=", 1)
            ->orderByDesc("sort")
            ->get(["title", "uid"])
            ->toArray();
    }

    // 获取分类seo信息
    public function getSeoInfo(string $uid): array
    {
        $defaultConfig = [
            "seo_title" => "程序员面试题",
            "seo_keywords" => "Java面试题，JavaScript面试题，PHP面试题，MySQL面试题，算法面试题，人工智能面试题，微信小程序答题，答题教育系统，答题小程序",
            "seo_description" => "兔兔答题是一款，专做程序员面试答题，丰富的面试资料。包含Java，Go，PHP，Rust，Python，前端，运维，人工智能，大数据等相关面试真题的微信小程序。",
        ];
        if (!empty($uid)) {
            $bean = UserCategory::query()
                ->where("uid", "=", $uid)
                ->first(["seo_title", "seo_keywords", "seo_description"]);
            if (!empty($bean)) {
                $bean = $bean->toArray();
                if (empty($bean["seo_title"])) {
                    $bean["seo_title"] = $defaultConfig["seo_title"];
                }
                if (empty($bean["seo_keywords"])) {
                    $bean["seo_keywords"] = $defaultConfig["seo_keywords"];
                }
                if (empty($bean["seo_description"])) {
                    $bean["seo_description"] = $defaultConfig["seo_description"];
                }
                return $bean;
            }
        }
        return $defaultConfig;
    }
}
