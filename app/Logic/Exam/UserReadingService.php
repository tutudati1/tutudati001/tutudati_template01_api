<?php
declare(strict_types=1);

namespace App\Logic\Exam;

use App\Library\SnowFlakeId;
use App\Logic\BaseUserService;
use App\Model\User\Exam\UserReading;
use App\Model\User\Exam\UserReadingCollectionHistory;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/23
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class UserReadingService extends BaseUserService
{
    public function submitCollection(): int
    {
        $requestParams = request()->all();
        $userCollection = UserReadingCollectionHistory::query()->where([
            ["user_uid", "=", $this->getUserUid()],
            ["reading_uid", "=", $requestParams["uid"]]
        ])->first(["id"]);
        if (!empty($userCollection)) {
            return 1;//已收藏
        }
        $model = UserReadingCollectionHistory::query()->create([
            "user_uid" => $this->getUserUid(),
            "reading_uid" => $requestParams["uid"],
            "uid" => SnowFlakeId::getId(),
        ]);
        if (!empty($model->getKey())) {
            UserReading::query()->where("uid", "=", $requestParams["uid"])->increment("collect");
            return 2; // 收藏成功
        }
        return 3;// 收藏失败
    }
}
