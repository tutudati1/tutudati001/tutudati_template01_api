<?php
declare(strict_types=1);

namespace App\Logic\Article;

use App\Model\User\Article\UserCategory;

/**
 * 文章分类
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/08/06
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class CategoryService
{
    // 获取文章分类
    public function getList():array
    {
        return UserCategory::query()
            ->where("is_show", "=", 1)
            ->orderByDesc("sort")
            ->orderByDesc("id")
            ->get(["uid", "title"])
            ->toArray();
    }
}
