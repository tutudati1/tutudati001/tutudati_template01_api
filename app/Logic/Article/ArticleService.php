<?php

declare(strict_types=1);

namespace App\Logic\Article;

use App\Library\SnowFlakeId;
use App\Logic\BaseUserService;
use App\Model\User\Article\UserArticle;
use App\Model\User\Article\UserCollect;
use Exception;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/23
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class ArticleService extends BaseUserService
{
    /**
     * 获取文章列表
     * @return array
     */
    public function getList(): array
    {
        $requestParams = request()->all();
        $items = UserArticle::query()
            ->with(["category:uid,title"])
            ->where([
                ["is_show", "=", 1]
            ])
            ->where(function ($query) use ($requestParams) {
                if ($requestParams["cate_uid"]) {
                    $query->where("cate_uid", "=", $requestParams["cate_uid"]);
                }
            })
            ->orderByDesc("id")
            ->select(["uid", "title", "read_count", "collect_count", "remark", "cate_uid", "url", "path", "keywords"])
            ->paginate($requestParams["size"] ?? 20);

        return [
            "items" => $items->items(),
            "page" => $items->currentPage(),
            "size" => (int)$requestParams["size"] ?? 20,
            "total" => $items->total(),
        ];
    }

    /**
     * 获取文章内容
     *
     * @return array
     */
    public function getContent(): array
    {
        $requestParams = request()->all();
        if (empty($requestParams["uid"])) {
            return [];
        }
        $bean = UserArticle::query()
            ->where([
                ["is_show", "=", 1],
                ["uid", "=", $requestParams["uid"]]
            ])
            ->first(["uid", "title", "read_count", "collect_count", "remark", "cate_uid", "content", "keywords"]);
        UserArticle::query()->where([
            ["uid", "=", $requestParams["uid"]]
        ])->increment("read_count");
        if (!empty($this->getUserUid())) {
            $userCollection = UserCollect::query()->where([
                ["user_uid", "=", $this->getUserUid()],
                ["article_uid", "=", $requestParams["uid"]]
            ])->first(["id"]);
            if (!empty($userCollection)) {
                $bean->is_collect = true;
            } else {
                $bean->is_collect = false;
            }
        }
        return !empty($bean) ? $bean->toArray() : [];
    }

    /**
     * 文章收藏
     * @return integer
     */
    public function submitCollect(): int
    {
        try {
            $requestParams = request()->all();
            UserCollect::query()->create([
                "user_uid" => $this->getUserUid(),
                "article_uid" => $requestParams["uid"],
                "uid" => SnowFlakeId::getId(),
            ]);
            UserArticle::query()->where("uid", "=", $requestParams["uid"])->increment("collect_count");
            return 2;
        } catch (Exception $e) {
            preg_match("/idx_ua/", $e->getMessage(), $str);
            if (!empty($str)) {
                return 1;
            }
            return 3;
        }
    }
}
