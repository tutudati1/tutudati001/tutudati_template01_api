<?php
declare(strict_types=1);

namespace App\Library;

use EasyWeChat\Factory;
use EasyWeChat\MiniProgram\Application;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/21
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class MiniWeChatClient
{
    public static function client(): Application
    {
        return Factory::miniProgram([
            'app_id' => env("WX_ID"),
            'secret' => env("WX_KEY"),
            'response_type' => 'array',
        ]);
    }
}
