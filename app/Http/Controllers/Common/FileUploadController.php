<?php
declare(strict_types=1);

namespace App\Http\Controllers\Common;

use App\Http\Controllers\BaseController;
use App\Logic\Com\UserFileUploadService;
use Illuminate\Http\JsonResponse;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/20
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class FileUploadController extends BaseController
{
    public function imageUpload(): JsonResponse
    {
        $fileInfo = (new UserFileUploadService())->imageUpload();
        if (isset($fileInfo["error"])) {
            return $this->error($fileInfo, $fileInfo["error"]);
        }
        return $this->success($fileInfo);
    }
}
