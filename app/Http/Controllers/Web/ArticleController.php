<?php
declare(strict_types=1);

namespace App\Http\Controllers\Web;

use App\Logic\Web\Article\ArticleService;
use App\Logic\Web\Article\CategoryService;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/27
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class ArticleController
{
    // 文章列表
    public function index($cate = 0)
    {
        $requestParams = request()->all();
        $title = $requestParams["s"] ?? "";
        $articleService = new ArticleService();
        $articleList = $articleService->getList(["cate_uid" => $cate, "title" => $title]);
        $hostList = $articleService->getHotList();
        $categoryList = [["uid" => "", "title" => "全部"]];
        array_push($categoryList, ...(new CategoryService())->getList());
        $seoInfo = (new CategoryService())->getSeoInfo((string)$cate);
        return view("web.article.index", [
            "articleList" => $articleList,
            "hotList" => $hostList,
            "categoryList" => $categoryList,
            "cate_uid" => $cate,
            "seoInfo" => $seoInfo,
        ]);
    }

    // 文章详情页
    public function content($id)
    {
        $articleService = new ArticleService();
        $content = $articleService->getContent(["uid" => $id]);
        $relList = $articleService->getRelList(["uid" => $id]);
        return view("web.article.content", [
            "content" => $content["content"],
            "next" => $content["next"],
            "pre" => $content["pre"],
            "relList" => $relList,
        ]);
    }
}
