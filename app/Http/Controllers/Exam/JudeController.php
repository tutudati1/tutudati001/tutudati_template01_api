<?php

declare(strict_types=1);

namespace App\Http\Controllers\Exam;

use App\Logic\Exam\JudeService;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Exam\CollectionCommonValidate;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/24
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class JudeController extends BaseController
{
    public function getList(CollectionCommonValidate $validate): JsonResponse
    {
        return $this->success((new JudeService)->getList());
    }
}
