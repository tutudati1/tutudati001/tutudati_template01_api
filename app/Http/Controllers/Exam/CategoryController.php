<?php

declare(strict_types=1);

namespace App\Http\Controllers\Exam;

use App\Http\Controllers\BaseController;
use App\Logic\Exam\CategoryService;
use Illuminate\Http\JsonResponse;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/20
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class CategoryController extends BaseController
{
    public function getList(): JsonResponse
    {
        return $this->success((new CategoryService())->getList());
    }
}
