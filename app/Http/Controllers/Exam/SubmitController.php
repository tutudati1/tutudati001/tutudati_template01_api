<?php
declare(strict_types=1);

namespace App\Http\Controllers\Exam;

use App\Http\Controllers\BaseController;
use App\Http\Requests\Exam\SubmitValidate;
use App\Logic\Exam\SubmitService;
use Illuminate\Http\JsonResponse;

/**
 * 考试答题记录
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/20
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class SubmitController extends BaseController
{
    // 提交试题作答
    public function submit(SubmitValidate $validate): JsonResponse
    {
        $submitResult = (new SubmitService())->submitService();
        if ($submitResult["errcode"] == 0) {
            return  $this->success($submitResult, $submitResult["errmsg"]);
        }
        return $this->error([], $submitResult["errmsg"]);
    }

    // 获取答题结果详情
    public function getSubmitDetail(): JsonResponse
    {
        $submitInfo = (new SubmitService())->getSubmitDetailService();
        if (count($submitInfo)) {
            return $this->success($submitInfo);
        }
        return $this->error();
    }

    // 获取答题历史记录
    public function getList(): JsonResponse
    {
        return $this->success((new SubmitService())->getHistoryList());
    }

    // 获取答题排名前10
    public function getSubmitRankList(): JsonResponse
    {
        return $this->success((new SubmitService())->getSubmitRankList());
    }
}
