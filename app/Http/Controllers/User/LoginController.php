<?php
declare(strict_types=1);

namespace App\Http\Controllers\User;

use App\Http\Controllers\BaseController;
use App\Logic\User\UserLoginService;
use Illuminate\Http\JsonResponse;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/19
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class LoginController extends BaseController
{
    public function login(): JsonResponse
    {
        $userLoginInfo = (new UserLoginService())->login();
        if ($userLoginInfo["errcode"] == 0) {
            return $this->success($userLoginInfo["data"], "登录成功");
        }
        return $this->error([], $userLoginInfo["errmsg"]);
    }
}
