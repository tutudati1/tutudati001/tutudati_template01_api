<?php
declare(strict_types=1);

namespace App\Http\Controllers\User;

use App\Http\Controllers\BaseController;
use App\Http\Requests\User\AccountLoginValidate;
use App\Http\Requests\User\UpdateInfoValidate;
use App\Logic\User\UserService;
use Illuminate\Http\JsonResponse;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/20
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class UserController extends BaseController
{
    public function updateUserInfo(UpdateInfoValidate $validate): JsonResponse
    {
        $updateUserInfo = (new UserService())->updateUserInfo();
        if ($updateUserInfo["errcode"] == 0) {
            return $this->success([], $updateUserInfo["errmsg"]);
        }
        return $this->error([], $updateUserInfo["errmsg"]);
    }

    public function getUserAvatarInfo(): JsonResponse
    {
        return $this->success((new UserService())->getUserAvatarInfo());
    }

    public function getUserBasicInfo(): JsonResponse
    {
        return $this->success((new UserService())->getUserBasicInfo());
    }

    /**
     * 检查用户登录状态
     *
     * @return JsonResponse
     */
    public function checkLogin(): JsonResponse
    {
        $userInfo = (new UserService)->checkoutUserLoginState();
        if (count($userInfo)) {
            return $this->success([], "登录状态");
        }
        return $this->error([], "未登录状态");
    }

    /**
     * 用户注册
     * @param AccountLoginValidate $validate
     * @return JsonResponse
     */
    public function register(AccountLoginValidate $validate): JsonResponse
    {
        $result = (new UserService())->accountRegister();
        if ($result["code"] == 0) {
            return $this->success([], $result["msg"]);
        }
        return $this->error([], $result["msg"]);
    }

    /**
     * 用户登录
     * @param AccountLoginValidate $validate
     * @return JsonResponse
     */
    public function login(AccountLoginValidate $validate): JsonResponse
    {
        $result = (new UserService())->accountLogin();
        if ($result["code"] == 0) {
            return $this->success([], $result["msg"]);
        }
        return $this->error([], $result["msg"]);
    }
}
