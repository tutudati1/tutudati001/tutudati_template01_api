<?php
declare(strict_types=1);

namespace App\Http\Controllers\Config;

use App\Http\Controllers\BaseController;
use App\Logic\Config\UserProfessionService;
use Illuminate\Http\JsonResponse;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/22
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class UserProfessionController extends BaseController
{
    public function getList(): JsonResponse
    {
        return $this->success((new UserProfessionService())->getList());
    }
}
