<?php
declare(strict_types=1);

namespace App\Http\Controllers\Config;

use App\Http\Controllers\BaseController;
use App\Logic\Config\BannerService;
use Illuminate\Http\JsonResponse;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/19
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class BannerController extends BaseController
{
    public function getList(): JsonResponse
    {
        return $this->success((new BannerService())->getList());
    }
}
