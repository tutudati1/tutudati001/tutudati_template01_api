<?php
declare(strict_types=1);

namespace App\Http\Controllers\Resource;

use App\Http\Controllers\BaseController;
use App\Logic\Resource\ResourceService;
use Illuminate\Http\JsonResponse;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/5/25
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class ResourceController extends BaseController
{
    public function getList(): JsonResponse
    {
        return $this->success((new ResourceService())->getList());
    }

    public function getContent(): JsonResponse
    {
        return $this->success((new ResourceService())->getContent());
    }

    public function getDownloadUrl(): JsonResponse
    {
        $downloadInfo = (new ResourceService())->getDownLoadUrl();
        if (count($downloadInfo)) {
            return $this->success($downloadInfo);
        }
        return $this->error([], "资源不存在");
    }
}
