<?php
declare(strict_types=1);

namespace App\Http\Controllers\Member;

use App\Http\Controllers\BaseController;
use App\Logic\Member\ConfigService;
use Illuminate\Http\JsonResponse;

/**
 * 会员等级配置
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/17
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class ConfigController extends BaseController
{
    // 会员权益列表
    public function getList(): JsonResponse
    {
        return $this->success((new ConfigService())->getList());
    }

    // 会员权益列表
    public function getContent(): JsonResponse
    {
        return $this->success((new ConfigService())->getContent());
    }
}
