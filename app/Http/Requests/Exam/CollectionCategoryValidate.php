<?php
declare(strict_types=1);

namespace App\Http\Requests\Exam;

use App\Http\Requests\BaseValidate;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/21
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class CollectionCategoryValidate extends BaseValidate
{
    public function rules(): array
    {
        return [
            "recommend" => "nullable|in:1,0",
            "hot" => "nullable|in:1,0",
        ];
    }

    public function messages(): array
    {
        return [
            "recommend.in" => "recommend参数错误",
            "hot.in" => "recommend参数错误",
        ];
    }
}
