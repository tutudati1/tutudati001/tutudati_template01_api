<?php
declare(strict_types=1);
/**
 * @project: 兔兔找盒
 * @author: Mandy
 * @date: 2023/10/9
 * @link: https://www.qqdeveloper.com/
 * @site: 微信搜索-兔兔找盒
 */

namespace App\Http\Requests\User;

use App\Http\Requests\BaseValidate;

class AccountLoginValidate extends BaseValidate
{
    public function rules(): array
    {
        return [
            "account" => "required|min:4|max:12",
            "password" => "required|alpha_dash|min:6|max:12",
        ];
    }

    public function messages(): array
    {
        return [
            "account.required" => "账号不能为空",
            "account.min" => "账号最小长度4位",
            "account.max" => "账号最小长度12位",
            "password.required" => "密码不能为空",
            "password.alpha_dash" => "密码必须由字母和数字组成",
            "password.min" => "密码最小长度6位",
            "password.max" => "密码最小长度12位",
        ];
    }
}
