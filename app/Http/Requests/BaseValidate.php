<?php
declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/21
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class BaseValidate extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }
}
