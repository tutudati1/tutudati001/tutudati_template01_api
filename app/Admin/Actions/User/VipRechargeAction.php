<?php
declare(strict_types=1);

namespace App\Admin\Actions\User;

use App\Model\Admin\User\AdminUser;
use Encore\Admin\Actions\Response;
use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/19
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class VipRechargeAction extends RowAction
{
    public $name = "设置会员";

    public function handle(Model $model): Response
    {
        $postParams = request()->all();
        $updateRow = AdminUser::query()->where("uid", "=", $postParams["uid"])->update([
            "is_vip" => $postParams["is_vip"],
            "vip_starttime" => $postParams["vip_starttime"],
            "vip_endtime" => $postParams["vip_endtime"],
        ]);
        if ($updateRow) {
            return $this->response()->refresh();
        }
        return $this->response()->error("设置失败");
    }

    public function form()
    {
        $this->hidden("uid", "会员uid")->default($this->getRow()->getAttribute("uid"));
        $this->select("is_vip", "设置会员")->default(2)->options([1 => "是", 2 => "否"]);
        $this->datetime("vip_starttime", "会员开始时间")->required();
        $this->datetime("vip_endtime", "会员结束时间")->required();
    }
}
