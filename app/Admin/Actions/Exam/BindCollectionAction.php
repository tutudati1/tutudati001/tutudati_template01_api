<?php
declare(strict_types=1);

namespace App\Admin\Actions\Exam;

use Encore\Admin\Actions\RowAction;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/19
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class BindCollectionAction extends RowAction
{
    public $name = "试题组卷";

    public function href(): string
    {
        return "/rabbit/exam/ex/all?collection_uid=". $this->getRow()->toArray()["uid"];
    }
}
