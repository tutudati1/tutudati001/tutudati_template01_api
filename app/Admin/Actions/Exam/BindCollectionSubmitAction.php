<?php
declare(strict_types=1);

namespace App\Admin\Actions\Exam;

use Encore\Admin\Actions\Response;
use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

/**
 * 试卷绑定提交
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/19
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class BindCollectionSubmitAction extends RowAction
{
    public $name = '绑定试卷';

    public function handle(Model $model, Request $request): Response
    {
        var_dump(\request()->all());
        return  $this->response()->success();
//        $requestParams = request()->all();
//        $collectionUid = Cache::get("admin:s:c:uid");
//        $uid = $requestParams["uid"];
//        $examType = $requestParams["exam_type"];
//        var_dump($requestParams, $collectionUid);
//        return $this->response()->success($uid . "||" . $examType . "||" . $collectionUid)->refresh();
    }

    public function form()
    {
        $this->confirm('确定添加到试卷?');
//        $this->hidden("uid", "试题uid")->default($this->row("uid"));
//        $this->hidden("exam_type", "试题类型")->default($this->row("exam_type"));
    }
}
