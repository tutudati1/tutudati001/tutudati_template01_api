<?php
declare(strict_types=1);

namespace App\Admin\Controllers\Message;

use App\Admin\Controllers\BaseController;
use App\Library\SnowFlakeId;
use App\Model\Admin\Message\AdminTemplateConfig;
use Encore\Admin\Form;
use Encore\Admin\Grid;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/8/03
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class TemplateConfigController extends BaseController
{
    protected $title = "模板配置";

    public function grid(): Grid
    {
        $grid = new Grid(new AdminTemplateConfig());
        $grid->filter(function ($filter) {
            $filter->disableIdFilter();
            $filter->column(1 / 2, function ($filter) {
                $filter->like("title", "模板名称");
            });
            $filter->column(1 / 2, function ($filter) {
                $filter->like("template_id", "微信模板");
            });
        });
        $grid->model()->orderByDesc("id")->paginate(15);
        $grid->column("uid", "数据编号");
        $grid->column("title", "模板名称")->editable();
        $grid->column("template_id", "模板编号")->editable()->copyable();
        $grid->column("count", "订阅总次数");
        $grid->column("group_count", "订阅总人数");
        $grid->column('is_show', "上架状态")->switch($this->switch);
        $grid->column("created_at", "创建时间");

        return $grid;
    }

    public function form(): Form
    {
        $form = new Form(new AdminTemplateConfig());
        $form->hidden("uid", "数据编号")->default(SnowFlakeId::getId());
        $form->text("title", "模板名称")->required()->help("为了快速查找，尽可能的与微信配置名称保持一致");
        $form->text("template_id", "模板编号")->required()->help("微信小程序管理平台，创建订阅消息获取到的模板id");
        $form->switch("is_show", "上架状态")->states($this->switch)->default(1);

        return $form;
    }
}
