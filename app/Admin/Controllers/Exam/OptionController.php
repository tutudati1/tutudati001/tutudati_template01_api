<?php
declare(strict_types=1);

namespace App\Admin\Controllers\Exam;

use App\Admin\Controllers\BaseController;
use App\Library\SnowFlakeId;
use App\Model\Admin\Exam\AdminCategory;
use App\Model\Admin\Exam\AdminCollection;
use App\Model\Admin\Exam\AdminOption;
use Encore\Admin\Form;
use Encore\Admin\Grid;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/5/24
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class OptionController extends BaseController
{
    protected $title = "选择试题";

    public function grid()
    {
        $grid = new Grid(new AdminOption());
        $grid->filter(function ($filter) {
            $filter->disableIdFilter();
            $filter->column(1 / 2, function ($filter) {
                $filter->like('main_title', "试题问题");
                $filter->equal('is_show', "上架状态")->select([
                    1 => '上架',
                    2 => '下架',
                ]);
            });
            $filter->column(1 / 2, function ($filter) {
                $filter->equal("type", "试题类型")->checkbox([1 => "单选试题", 2 => "多选试题"]);
                $filter->between('created_at', "创建时间")->datetime();
            });
        });
        $grid->model()->orderByDesc("id")->paginate(20);
        $grid->column("uid", "数据编号")->copyable();
        $grid->column("category.title", "试题分类");
        $grid->column("type", "试题类型")->display(function ($type) {
            $typeConfig = [1 => "单选试题", 2 => "多选试题"];
            if ($type == 1) {
                return "<span style='color: #00a65a;'>$typeConfig[$type]</span>";
            }
            return "<span style='color: #7759da;'>$typeConfig[$type]</span>";
        });
        //$grid->column("collection.title", "试卷名称");
        $grid->column("title", "试题题干")->editable();
        $grid->column("answer", "试题答案")->label("info");
        $grid->column('is_show', "上架状态")->switch($this->switch);
        $grid->column("sort", "显示权重")->integer();
        $grid->column("score", "试题积分");
        $grid->column("created_at", "创建时间");
        $grid->disableExport();
        $grid->actions(function ($actions) {
            $actions->disableView();
        });

        return $grid;
    }

    public function form(): Form
    {
        $form = new Form(new AdminOption());
        $form->hidden("uid", "试卷编号")->default(SnowFlakeId::getId());
        $form->radio("category_uid", "试题分类")->options(AdminCategory::getList())->required();
        $form->multipleSelect("collection", "试题试卷")->options(AdminCollection::getList());
        $form->text("title", "试题题干")->rules('required|max:255')->help("最大字符长度255");
        $form->UEditor('second_title', "二级题干")->help("适用于题干中带有图片，视频等场景")->options(['initialFrameHeight' => 200]);
        $form->table('option', "试题选项", function ($form) {
            $form->radio('key1', "答案")->options([1 => "是", 2 => "否"])->default(2)->rules('required');
            $form->text('key2', "选项内容")->rules('required');
        });
        $form->decimal("score", "试题积分")->help("试题积分不能小于0")->required()->value(5);
        $form->number("sort", "显示权重")->default(0)->help("值越大，显示权重越高");
        $form->switch("is_show", "上架状态")->states($this->switch)->default(1);
        $form->UEditor('analysis', "试题解析")->options(['initialFrameHeight' => 400]);
        $form->hidden("type", "试题类型")->default(1);
        $form->hidden("answer", "试题答案")->with(function ($value) {
            if (empty($value)) {
                return "";
            }
            return implode(",", $value);
        });
        $form->saving(function (Form $form) {
            $options = $form->option;
            $answer = [];
            $letter = ["A", "B", "C", "D", "E", "F", "G", "H"];
            $i = 0;
            foreach ($options as $value) {
                if ($value["key1"] == 1) {
                    $answer[] = $letter[$i];
                }
                ++$i;
            }
            sort($answer);
            $form->answer = json_encode($answer, JSON_UNESCAPED_UNICODE);
            if (count($answer) > 1) {
                $form->type = 2;
            } else {
                $form->type = 1;
            }
        });

        return $form;
    }
}
