<?php
declare(strict_types=1);

namespace App\Admin\Controllers\Resource;

use App\Admin\Controllers\BaseController;
use App\Library\SnowFlakeId;
use App\Model\Admin\Resource\AdminCategory;
use Encore\Admin\Form;
use Encore\Admin\Grid;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/5/16
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class CategoryController extends BaseController
{
    protected $title = "资源类目";

    public function grid(): Grid
    {
        $grid = new Grid(new AdminCategory());
        $grid->filter(function ($filter) {
            $filter->disableIdFilter();
            $filter->column(1 / 2, function ($filter) {
                $filter->like('title', "分类名称");
                $filter->equal('is_show', "上架状态")->select([
                    1 => '上架',
                    2 => '下架',
                ]);
            });
            $filter->column(1 / 2, function ($filter) {
                $filter->equal('parent_uid', "上级分类")->select(AdminCategory::getList());
                $filter->between('created_at', "创建时间")->datetime();
            });
        });
        $grid->model()->orderByDesc("id");
        $grid->column("uid", "数据编号")->copyable();
        $grid->column("cover", "分类封面")->lightbox(['width' => 100, 'height' => 100]);
        $grid->column("parent.title", "上级分类");
        $grid->column("title", "分类名称")->editable();
        $grid->column("sort", "显示权重")->integer()->sortable();
        $grid->column('is_show', "上架状态")->switch($this->switch);
        $grid->column("created_at", "创建时间");
        $grid->disableExport();
        $grid->actions(function ($actions) {
            $actions->disableView();
        });

        return $grid;
    }

    public function form(): Form
    {
        $form = new Form(new AdminCategory());
        $form->hidden("uid", "分类编号")->default(SnowFlakeId::getId());
        $form->hidden("url", "图片地址")->default(env("QINIU_URL"));
        $form->image("path", "分类封面")->uniqueName();
        $form->select("parent_uid", "上级分类")->default(0)->options(AdminCategory::getList());
        $form->text("title", "分类名称")->rules('required|max:20');
        $form->number("sort", "显示权重")->default(0)->help("值越大，显示权重越高");
        $form->switch("is_show", "上架状态")->states($this->switch)->default(1);

        return $form;
    }
}
