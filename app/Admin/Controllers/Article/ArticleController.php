<?php
declare(strict_types=1);
/**
 * @project: 兔兔兔兔答题微信小程序
 * @author: kert
 * @date: 2023/5/23
 * @link: http://www.tutudati.com/
 * @site: 微信搜索-兔兔答题
 */

namespace App\Admin\Controllers\Article;

use App\Admin\Controllers\BaseController;
use App\Library\SnowFlakeId;
use App\Model\Admin\Article\Article;
use App\Model\Admin\Article\ArticleCate;
use Encore\Admin\Form;
use Encore\Admin\Grid;

class ArticleController extends BaseController
{
    protected $title = "文章内容";

    public function grid(): Grid
    {
        $grid = new Grid(new Article());
        $grid->filter(function ($filter) {
            $filter->disableIdFilter();
            $filter->column(1 / 2, function ($filter) {
                $filter->like('title', "文章标题");
                $filter->equal('is_show', "上架状态")->select([
                    1 => '上架',
                    2 => '下架',
                ]);
            });
            $filter->column(1 / 2, function ($filter) {
                $filter->between('created_at', "创建时间")->datetime();
            });
        });
        $grid->model()->orderByDesc("id")->paginate(10);

        $grid->column("uid", "数据编号")->copyable();
        $grid->column("title", "文章标题")->editable();
        $grid->column("author", "文章作者")->editable();
        $grid->column("cover", "文章封面")->lightbox(['width' => 100, 'height' => 100]);
        $grid->column("category.title", "文章分组");
        $grid->column("sort", "显示顺序")->sortable()->editable();
        $grid->column("collect_count", "收藏数量")->sortable()->editable();
        $grid->column("read_count", "阅读数量")->sortable()->editable();
        $grid->column('is_show', "上架状态")->switch($this->switch);
        $grid->column("keywords", "文章标签")->label("info");
        $grid->column("created_at", "创建时间");
        $grid->disableExport();
        $grid->actions(function ($actions) {
            $actions->disableView();
        });
        return $grid;
    }

    public function form(): Form
    {
        $form = new Form(new Article());
        $form->hidden("uid", "文章编号")->default(SnowFlakeId::getId());
        $form->radio("cate_uid", "文档分组")->options(ArticleCate::list())->required();
        $form->hidden("url", "图片地址")->default(env("QINIU_URL"));
        $form->image("path", "文章封面")->uniqueName()->required();
        $form->text("author", "文章作者")->default("兔兔答题")->required();
        $form->url("office_url", "官方网站")->default(env("APP_URL"))->required();
        $form->text("title", "文章标题")->rules('required|max:100')->help("文章标题字符长度不能超过100");
        $form->number("sort", "显示顺序")->default(0)->help("值越大，显示权重越高，最大值99999999");
        $form->switch("is_show", "上架状态")->options($this->switch)->default(1);
        $form->textarea("remark", "文章简介")->rules('required|max:255')->help("文章简介字符长度不能超过255")->required();
        $form->tags("keywords", "文章标签")->help("推荐标签数量在3个以内");
        $form->UEditor('content', "文档内容")->options(['initialFrameHeight' => 400])->required();

        return $form;
    }
}
