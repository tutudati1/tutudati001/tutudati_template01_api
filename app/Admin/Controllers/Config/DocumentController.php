<?php
declare(strict_types=1);
/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/9
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */

namespace App\Admin\Controllers\Config;

use App\Admin\Controllers\BaseController;
use App\Library\SnowFlakeId;
use App\Model\Admin\Config\AdminDocument;
use Encore\Admin\Form;
use Encore\Admin\Grid;

class DocumentController extends BaseController
{
    protected $title = "文章配置";

    public function grid(): Grid
    {
        $grid = new Grid(new AdminDocument());
        $grid->filter(function ($filter) {
            $filter->disableIdFilter();
            $filter->like('title', "文章标题");
            $filter->equal('is_show', "上架状态")->radio([
                1 => '上架',
                2 => '下架',
            ]);
        });
        $grid->column("uid", "数据编号")->copyable();
        $grid->column("title", "文章标题");
        $grid->column('is_show', "上架状态")->switch($this->switch);
        $grid->column("created_at", "创建时间");
        $grid->column("updated_at", "更新时间");
        $grid->disableExport();
        $grid->actions(function ($actions) {
            $actions->disableView();
        });

        return $grid;
    }

    public function form(): Form
    {
        $form = new Form(new AdminDocument());
        $form->hidden("uid", "文章编号")->default(SnowFlakeId::getId());
        $form->text("title", "文章标题")->rules('required|max:20')->required();
        $form->switch("is_show", "上架状态")->states($this->switch)->default(1);
        $form->UEditor('content', "文档内容")->options(['initialFrameHeight' => 400])->required();

        return $form;
    }
}
