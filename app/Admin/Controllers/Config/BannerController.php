<?php
declare(strict_types=1);
/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/9
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */

namespace App\Admin\Controllers\Config;

use App\Admin\Controllers\BaseController;
use App\Library\SnowFlakeId;
use App\Model\Admin\Config\AdminBanner;
use Encore\Admin\Form;
use Encore\Admin\Grid;

class BannerController extends BaseController
{
    protected $title = "轮播图管理";

    public function grid(): Grid
    {
        $grid = new Grid(new AdminBanner());
        $grid->model()->orderByDesc("id")->paginate(10);
        $grid->disableFilter();
        $grid->column("uid", "数据编号")->copyable();
        $grid->column("position", "显示位置")->select($this->bannerOption);
        $grid->column("first_title", "一级标题");
        $grid->column("second_title", "二级标题");
        $grid->column("cover", "图片封面")->lightbox(['width' => 150, 'height' => 100]);
        $grid->column("sort", "显示权重")->integer();
        $grid->column("navigate", "跳转地址");
        $grid->column('is_show', "上架状态")->switch($this->switch);
        $grid->column("created_at", "创建时间");
        $grid->disableExport();
        $grid->actions(function ($actions) {
            $actions->disableView();
        });

        return $grid;
    }

    public function form(): Form
    {
        $form = new Form(new AdminBanner());
        $form->hidden("uid", "图片编号")->default(SnowFlakeId::getId());
        $form->select("position", "显示位置")->required()->options($this->bannerOption);
        $form->text("first_title", "一级标题")->rules('sometimes|max:20')
            ->help("最大不要超过20个字符，当只支持一个标题的场景，优先使用主标题");
        $form->text("second_title", "二级标题")->rules('sometimes|max:20')->help("最大不要超过20个字符");
        $form->text("navigate", "跳转地址");
        $form->hidden("url", "图片地址")->default(env("QINIU_URL"));
        $form->number("sort", "显示权重")->default(0)->rules("required|integer")->help("权重越高，显示越靠前");
        $form->switch("is_show", "上架状态")->states($this->switch)->default(1);
        $form->image("path", "图片封面")->uniqueName()->required();

        return $form;
    }
}
