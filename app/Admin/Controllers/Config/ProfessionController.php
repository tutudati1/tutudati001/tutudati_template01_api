<?php
declare(strict_types=1);
/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/9
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */

namespace App\Admin\Controllers\Config;

use App\Admin\Controllers\BaseController;
use App\Library\SnowFlakeId;
use App\Model\Admin\Config\AdminProfession;
use Encore\Admin\Form;
use Encore\Admin\Grid;

class ProfessionController extends BaseController
{
    protected $title = "行业配置";

    public function grid(): Grid
    {
        $grid = new Grid(new AdminProfession());
        $grid->model()->orderByDesc("id");
        $grid->disableFilter();

        $grid->column("uid", "数据编号")->copyable();
        $grid->column("title", "行业名称")->editable();
        $grid->column('is_show', "上架状态")->switch($this->switch);
        $grid->column("sort", "显示权重")->integer();
        $grid->column("created_at", "创建时间");
        $grid->column("updated_at", "更新时间");
        $grid->actions(function ($actions) {
            $actions->disableView();
        });

        return $grid;
    }

    public function form(): Form
    {
        $form = new Form(new AdminProfession());
        $form->hidden("uid", "数据编号")->default(SnowFlakeId::getId());
        $form->text("title", "行业名称")->rules('required|max:20');
        $form->switch("is_show", "上架状态")->states($this->switch)->default(1);
        $form->number("sort", "显示权重")->default(0)->rules("required|integer")->help("权重越高，显示越靠前");

        return $form;
    }
}
