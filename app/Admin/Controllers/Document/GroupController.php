<?php
declare(strict_types=1);

namespace App\Admin\Controllers\Document;

use App\Admin\Controllers\BaseController;
use App\Library\SnowFlakeId;
use App\Model\Admin\Document\AdminDocumentGroup;
use Encore\Admin\Form;
use Encore\Admin\Grid;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/9
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class GroupController extends BaseController
{
    protected $title = "文档分组";

    public function grid(): Grid
    {
        $grid = new Grid(new AdminDocumentGroup());
        $grid->model()->orderByDesc("id");
        $grid->disableFilter();

        $grid->column("uid", "分组编号")->copyable();
        $grid->column("title", "分组标题")->editable();
        $grid->column('is_show', "上架状态")->switch($this->switch);
        $grid->column("sort", "显示权重")->integer();
        $grid->column("created_at", "创建时间");
        $grid->column("updated_at", "更新时间");
        $grid->disableExport();
        $grid->actions(function ($actions) {
            $actions->disableView();
        });

        return $grid;
    }

    public function form(): Form
    {
        $form = new Form(new AdminDocumentGroup());
        $form->hidden("uid", "分组编号")->default(SnowFlakeId::getId());
        $form->text("title", "分组标题")->rules('required|max:20')->required();
        $form->switch("is_show", "上架状态")->options($this->switch)->default(1);
        $form->number("sort", "显示权重")->max(10000)->min(1)->default(1)->help("值越大，权重越高");

        return $form;
    }
}
