<?php
declare(strict_types=1);


namespace App\Admin\Controllers\Document;

use App\Admin\Controllers\BaseController;
use App\Library\SnowFlakeId;
use App\Model\Admin\Document\AdminDocument;
use App\Model\Admin\Document\AdminDocumentGroup;
use Encore\Admin\Form;
use Encore\Admin\Grid;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/9
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class DocumentController extends BaseController
{
    protected $title = "帮助文档";

    public function grid(): Grid
    {
        $grid = new Grid(new AdminDocument());
        $grid->filter(function ($filter) {
            $filter->disableIdFilter();
            $filter->column(1 / 2, function ($filter) {
                $filter->like('title', "文章标题");
                $filter->equal('is_show', "上架状态")->select([
                    1 => '上架',
                    2 => '下架',
                ]);
            });
            $filter->column(1 / 2, function ($filter) {
                $filter->equal('group_uid', "文章分组")->select(AdminDocumentGroup::list());
                $filter->between('created_at', "创建时间")->datetime();
            });
        });
        $grid->model()->orderByDesc("id");

        $grid->column("uid", "数据编号")->copyable();
        $grid->column("group.title", "文章分组");
        $grid->column("title", "文章标题")->editable();
        $grid->column('is_show', "上架状态")->switch($this->switch);
        $grid->column("sort", "显示权重")->integer();
        $grid->column("created_at", "创建时间");
        $grid->column("updated_at", "更新时间");
        $grid->disableExport();
        $grid->actions(function ($actions) {
            $actions->disableView();
        });

        return $grid;
    }

    public function form(): Form
    {
        $form = new Form(new AdminDocument());
        $form->select("group_uid", "文档分组")->options(AdminDocumentGroup::list());
        $form->hidden("uid", "文章编号")->default(SnowFlakeId::getId());
        $form->text("title", "文章标题")->rules('required|max:32')->required()->help("标题最大32个字符");
        $form->switch("is_show", "上架状态")->options($this->switch)->default(1);
        $form->number("sort", "显示权重")->max(10000)->min(1)->default(1)->help("值越大，权重越高");
        $form->UEditor('content', "文档内容")->options(['initialFrameHeight' => 400])->required();

        return $form;
    }
}
