<?php
declare(strict_types=1);
/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/9
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */

namespace App\Admin\Controllers\User;

use App\Admin\Actions\User\VipRechargeAction;
use App\Admin\Controllers\BaseController;
use App\Model\Admin\Config\AdminProfession;
use App\Model\Admin\User\AdminUser;
use Encore\Admin\Grid;

class UserController extends BaseController
{
    protected $title = "用户列表";

    public function grid(): Grid
    {
        $grid = new Grid(new AdminUser());
        $grid->filter(function ($filter) {
            $filter->disableIdFilter();
            $filter->column(1 / 2, function ($filter) {
                $filter->like('nickname', "用户昵称");
                $filter->like('name', "用户姓名");
                $filter->like('email', "邮箱账号");
                $filter->equal("profession_uid", "用户职业")->select(AdminProfession::getList());
            });
            $filter->column(1 / 2, function ($filter) {
                $filter->between('created_at', "注册时间")->datetime();
                $filter->between('birthday', "用户生日")->date();
                $filter->equal("gender", "用户性别")->select(['保密', '男', '女']);
                $filter->equal("is_vip", "会员用户")->select([1 => "会员用户", 2 => "普通用户"]);
            });
        });
        $grid->model()->orderByDesc("id")->paginate(15);
        $grid->column("uid", "用户编号")->copyable();
        $grid->column("avatar_url", "用户头像")->lightbox(['width' => 50, 'height' => 50]);
        $grid->column("nickname", "用户昵称");
        $grid->column("account", "登录账号");
        $grid->column("name", "用户姓名");
        $grid->column("openid", "微信openid")->copyable();
        $grid->column("mobile", "手机号");
        $grid->column("email", "邮箱");
        $grid->column("is_vip", "会员用户")->display(function ($is_vip) {
            if ($is_vip == 1) {
                return "<span style='color:blue'>是</span>";
            }
            return "<span style='color:gray'>否</span>";
        });
        $grid->column("vip_starttime", "会员开始时间");
        $grid->column("vip_endtime", "会员结束时间");
        $grid->column("created_at", "注册时间");

        $grid->actions(function ($actions) {
            $actions->disableView();
            $actions->disableDelete();
            $actions->disableEdit();
            $actions->add(new VipRechargeAction());
        });
        $grid->disableCreateButton();
        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
        });

        return $grid;
    }
}
