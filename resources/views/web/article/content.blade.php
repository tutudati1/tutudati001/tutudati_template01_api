<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <meta name="renderer" content="webkit">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    @if(!empty($content))
        <title>{{env("APP_SITE_NAME")}} - {{$content["title"]}}</title>
        <meta name="description"
              content="{{$content["remark"]}}">
        <meta name="keywords"
              content="{{$content["title"]}}@php echo "," . implode(",", $content["keywords"]); @endphp">
    @else
        <title>{{env("APP_SITE_NAME")}} - 程序员面试题宝典</title>
        <meta name="description"
              content="兔兔答题是一款，专做程序员面试答题，丰富的面试资料。包含Java，Go，PHP，Rust，Python，前端，运维，人工智能，大数据等相关面试真题的微信小程序。">
        <meta name="keywords"
              content="Java面试题，JavaScript面试题，PHP面试题，MySQL面试题，算法面试题，人工智能面试题，微信小程序答题，答题教育系统，答题小程序">
    @endif
    <link rel="shortcut icon" href="{{env("APP_URL")}}image/favicon.ico">
    <link rel="apple-touch-icon-precomposed" href="{{env("APP_URL")}}image/logo.png"/>
    <link rel="apple-touch-icon" sizes="76x76" href="{{env("APP_URL")}}image/logo.png"/>
    <link rel="apple-touch-icon" sizes="120x120" href="{{env("APP_URL")}}image/logo.png"/>
    <link rel="apple-touch-icon" sizes="152x152" href="{{env("APP_URL")}}image/logo.png"/>
    <link rel="apple-touch-icon" sizes="180x180" href="{{env("APP_URL")}}image/logo.png"/>
    <meta content='{{env("APP_SITE_NAME")}}' name='Author'/>

    <link href="{{env("APP_URL")}}web/css/style.css" rel="stylesheet" type="text/css"/>

    <meta name='robots' content='max-image-preview:large'/>

    <style type="text/css">
        img.wp-smiley, img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 0.07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>

<body class="home blog">
<div class="box-wrap">
    {{--    左侧菜单导航开始--}}
    <div class="header">
        <div class="logo-wrap">
            <a href="{{env("APP_URL")}}" title="专做程序员面试题" class="logo-img-wrap">
                <img src="{{env("APP_URL")}}image/logo.png" alt="兔兔答题" class="logo">
                <h1>兔兔答题</h1>
            </a>
            <div class="sub-title">专做程序员面试题</div>
        </div>

        <div class="menu-header-container">
            <ul id="menu-header" class="menu">
                <li id="menu-item-23"
                    class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-23">
                    <a title="程序面试题" href="{{env("APP_URL")}}">首页</a>
                </li>
                <li id="menu-item-25" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-25">
                    <a title="程序技术文章" href="{{env("APP_URL")}}collection/list">答题</a>
                </li>
                <li id="menu-item-3667"
                    class="menu-item menu-item-type-taxonomy menu-item-object-category current-menu-item menu-item-3667">
                    <a title="程序员交流" href="{{env("APP_URL")}}article/list">资讯</a>
                </li>
                <li id="menu-item-24" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-24">
                    <a title="程序员推荐" href="{{env("APP_URL")}}article/list">推荐</a>
                </li>
            </ul>
        </div>

        <div class="wx-code">
            <i class="tficon icon-qrcode"></i>
            扫码答题
            <img src="{{env("WEB_MENU_LOGO")}}" alt="兔兔答题微信小程序" class="wx-code-img">
        </div>
    </div>
    {{--    左侧菜单导航结束--}}

    {{--    主体内容开始--}}
    <div class="main">
        <div class="top-bar">
            <div class="crumbs">
                <a href="{{env("APP_URL")}}">首页</a>
                <span>资讯详情</span>
            </div>
            {{--            <form class="search-form" method="get" action="{{env("app_url")}}">--}}
            {{--                <button class="tficon icon-search" type="submit"></button>--}}
            {{--                <input type="text" name="s" class="search-input" placeholder="输入关键词，回车搜索" value="">--}}
            {{--            </form>--}}
        </div>

        {{--        <ins class="adsbygoogle mobile-hide-sense" style="display:block; text-align:center;" data-ad-layout="in-article"--}}
        {{--             data-ad-format="fluid" data-ad-client="ca-pub-4401169466922752" data-ad-slot="5079798331"></ins>--}}
        {{--        <script>--}}
        {{--            (adsbygoogle = window.adsbygoogle || []).push({});--}}
        {{--        </script>--}}


        <div class="post-wrap" id="post-wrap">
            <div class="post-header">
                <h1 class="post-title">{{$content["title"]}}</h1>
                <div class="post-meta">
                    <ul class="post-categories">
                        <li><a href="{{request()->url()}}" rel="category tag">{{$content["category"]["title"]}}</a></li>
                    </ul>
                    @php echo date("Y-m-d", strtotime($content["created_at"])); @endphp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <i class="tficon icon-fire-line"></i> {{$content["read_count"]}}
                    <div class="tag-wrap post-header-tags">
                        @foreach($content["keywords"] as $value)
                            <a href="{{request()->url()}}" rel="tag">{{$value}}</a>
                        @endforeach
                    </div>
                </div>
            </div>


            <div class="post-content" id="post-content">
                <img src="{{$content["url"]}}{{$content["path"]}}"
                     class="wp-post-image" alt="{{$content["title"]}}"
                     width="100%" height="auto" style="border-radius: 10px;">
                <p class="single-excerpt">{{$content["remark"]}}</p>


                <!-- 横条广告 -->
                {{--                <ins class="adsbygoogle"--}}
                {{--                     style="display:inline-block;min-width:400px;max-width:720px;width:100%;height:260px"--}}
                {{--                     data-ad-client="ca-pub-4401169466922752"--}}
                {{--                     data-ad-slot="8614034451"--}}
                {{--                     data-ad-format="auto"--}}
                {{--                     data-full-width-responsive="true"></ins>--}}
                {{--                <script>--}}
                {{--                    (adsbygoogle = window.adsbygoogle || []).push({});--}}
                {{--                </script>--}}
                {!! $content["content"] !!}

            </div>


            <!-- 横条广告 -->
            {{--            <ins class="adsbygoogle"--}}
            {{--                 style="display:inline-block;min-width:400px;max-width:720px;width:100%;height:260px"--}}
            {{--                 data-ad-client="ca-pub-4401169466922752"--}}
            {{--                 data-ad-slot="8614034451"--}}
            {{--                 data-ad-format="auto"--}}
            {{--                 data-full-width-responsive="true"></ins>--}}
            {{--            <script>--}}
            {{--                (adsbygoogle = window.adsbygoogle || []).push({});--}}
            {{--            </script>--}}
            <div class="post-list">
                @if(!empty($pre))
                    <div class="post-item">
                        <div class="post-item-cover">
                            <a class="post-item-img" href="{{env("APP_URL")}}article/detail/{{$pre["uid"]}}"
                               title="{{$pre["title"]}}"><img
                                    class="hover-scale"
                                    src="{{$pre["url"]}}{{$pre["path"]}}"
                                    alt="{{$pre["title"]}}">
                                <h5 class="single-prev-text"><i class="tficon icon-left"></i> 上一篇</h5>
                            </a>
                        </div>
                        <a href="{{env("APP_URL")}}article/detail/{{$pre["uid"]}}" class="post-item-title"
                           title="{{$pre["title"]}}">
                            <h3>{{$pre["title"]}}</h3>
                        </a>
                    </div>
                @endif
                @if(!empty($next))
                    <div class="post-item">
                        <div class="post-item-cover">
                            <a class="post-item-img" href="{{env("APP_URL")}}article/detail/{{$next["uid"]}}"
                               title="{{$next["title"]}}"><img
                                    class="hover-scale"
                                    src="{{$next["url"]}}{{$next["path"]}}"
                                    alt="{{$next["title"]}}">
                                <h5 class="single-next-text">下一篇 <i class="tficon icon-right"></i></h5>
                            </a>
                        </div>
                        <a href="{{env("APP_URL")}}article/detail/{{$next["uid"]}}" class="post-item-title"
                           title="{{$next["title"]}}">
                            <h3>{{$next["title"]}}</h3>
                        </a>
                    </div>
                @endif
            </div>
        </div>
    </div>
    {{--主体内容结束--}}

    {{--    侧边栏开始--}}
    <div class="aside" id="single-sidebar">
        <div class="aside-block" id="single-aside-content">
            <div class="block-wrap"><h2 class="block-title">访问官网</h2><a
                    href="{{$content["office_url"]}}" class="side-btn" target="_blank"
                    rel="nofollow"><i class="tficon icon-web"></i>
                    <div class="side-btn-text">{!! $content["office_url"] !!}</div>
                </a></div>
            <div class="block-wrap">
                <h2 class="block-title">
                    文章目录
                </h2>

                <ul class="directory" id="directory"></ul>

            </div>
        </div>


        {{--        <div class="aside-block aside-ddd block-wrap mobile-hide-sense">--}}

        {{--            <!-- 侧栏方形广告 -->--}}
        {{--            <ins class="adsbygoogle aside-ddd-square"--}}
        {{--                 style="display:block"--}}
        {{--                 data-ad-client="ca-pub-4401169466922752"--}}
        {{--                 data-ad-slot="4679734589"--}}
        {{--                 data-ad-format="auto"--}}
        {{--                 data-full-width-responsive="true"></ins>--}}
        {{--            <script>--}}
        {{--                (adsbygoogle = window.adsbygoogle || []).push({});--}}
        {{--            </script>--}}
        {{--        </div>--}}


        <div class="aside-block block-wrap">
            <div class="single-relative">
                <h2 class="block-title">
                    更多相关文章
                </h2>
                <div class="aside-post-list">
                    @foreach($relList as $value)
                        <div class="post-item">
                            <div class="post-item-cover">
                                <a class="post-item-img" href="{{env("APP_URL")}}article/detail/{{$value["uid"]}}"
                                   title="{{$value["title"]}}"><img class="hover-scale"
                                                                    src="{{$value["url"]}}{{$value["path"]}}"
                                                                    alt="{{$value["title"]}}"></a>
                                {{--                                <ul class="post-categories">--}}
                                {{--                                    <li><a href="design/photos.html" rel="category tag">免费商用图库</a></li>--}}
                                {{--                                </ul>--}}
                            </div>
                            <a href="{{env("APP_URL")}}article/detail/{{$value["uid"]}}" class="post-item-title"
                               title="{{$value["title"]}}">
                                <h3>{{$value["title"]}}</h3>
                            </a>
                            {{--                            <div class="post-item-footer">--}}
                            {{--                                <div class="tag-wrap">--}}
                            {{--                                    <a href="tag/3d.html" rel="tag">3d</a><a href="tag/emoji.html" rel="tag">emoji</a><a--}}
                            {{--                                        href="tag/figma.html" rel="tag">figma</a><a href="tag/mit.html"--}}
                            {{--                                                                                    rel="tag">MIT开源协议</a><a--}}
                            {{--                                        href="tag/png.html" rel="tag">png</a><a href="tag/svg.html" rel="tag">svg</a><a--}}
                            {{--                                        href="tag/people-photo.html" rel="tag">人物图库</a><a href="tag/stock.html"--}}
                            {{--                                                                                              rel="tag">素材</a></div>--}}
                            {{--                                <div class="post-item-meta">2022-08-23</div>--}}
                            {{--                            </div>--}}
                            <p class="post-item-summary">{{$value["remark"]}}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        {{--        <div class="aside-block aside-ddd block-wrap single-ddd">--}}

        {{--            <!-- 侧栏竖条广告 -->--}}
        {{--            <ins class="adsbygoogle aside-ddd-clum"--}}
        {{--                 style="display:block"--}}
        {{--                 data-ad-client="ca-pub-4401169466922752"--}}
        {{--                 data-ad-slot="4195151876"--}}
        {{--                 data-ad-format="auto"--}}
        {{--                 data-full-width-responsive="true"></ins>--}}
        {{--            <script>--}}
        {{--                (adsbygoogle = window.adsbygoogle || []).push({});--}}
        {{--            </script>--}}
        {{--        </div>--}}

        <div class="aside-block block-wrap" style="position: sticky;top: 0;">
            <div id="append-aside"></div>
            <div class="wwads-cn wwads-horizontal" data-id="195" style="max-width:360px"></div>
        </div>
    </div>
    {{--    侧边栏结束--}}

    <link href="{{env("APP_URL")}}web/js/monokai-sublime.min.css" rel="stylesheet" type="text/css">
    {{--    先去掉该js，因为使用的是墨滴的编辑器，会导致代码全部在一行。--}}
    {{--    <script src="{{env("APP_URL")}}web/js/highlight.min.js"></script>--}}
    <script>
        // 代码美化相关
        // hljs.highlightAll();
        // 文章目录相关
        const h3list = document.querySelectorAll('.post-content h2');
        let list = '';
        let h3tops = []; // 每个h3标题距离顶部的位置
        h3list.forEach(function (item, index) {
            list += '<li onclick="toH3(' + index + ')">' + item.innerHTML + '</li>';
            h3tops.unshift(item.offsetTop); // 反着排，方便滚动计算
        });
        document.getElementById('directory').innerHTML = list;

        function toH3(index) {
            let topx = h3list[index].offsetTop - 8;
            window.scrollTo({
                top: topx,
                behavior: "smooth"
            });
        }

        // 监听滚动结束
        let t1 = 0;
        let t2 = 0;
        let timer = null;
        let indexList = document.querySelectorAll('#directory li');

        // scroll监听
        document.onscroll = function () {
            clearTimeout(timer);
            timer = setTimeout(isScrollEnd, 500);
            t1 = document.documentElement.scrollTop || document.body.scrollTop;
        }

        let h3Len = h3tops.length;

        function isScrollEnd() {
            t2 = document.documentElement.scrollTop || document.body.scrollTop;
            if (t2 == t1) {
                //console.log('滚动结束了');
                let ttt = (document.documentElement.scrollTop || document.body.scrollTop) + 198;

                let activeIndex = h3tops.findIndex(function (top) {
                    return ttt >= top
                });
                let _index = h3Len - activeIndex - 1;

                //console.log(_index);
                let a = document.querySelector('#directory .active');
                if (a !== null) a.className = '';
                // 至少滚到第一个标题才显示
                if (ttt >= h3tops[h3Len - 1]) indexList[_index].className = 'active';
            }
        }

        // ---------------------------- 手机端处理

        // 把侧栏移动到文章里面
        function move_sidebar() {
            let wrap = document.querySelector('.post-content');
            let cont = document.querySelector('.post-content h3');
            let sidebar = document.getElementById('single-aside-content');
            wrap.insertBefore(sidebar, cont);
        }

        // 判断手机端
        var innerW = window.innerWidth;
        if (innerW < 569) {
            move_sidebar();
            document.onscroll = null;
        } else {
            // 复制一份到侧栏最后
            document.getElementById('append-aside').innerHTML = document.getElementById('single-aside-content').innerHTML;
            document.querySelector('#append-aside .block-wrap').style.marginTop = 0;
        }
    </script>
</div>
<div class="footer">
    Copy Right©2023, All Rights Reserved. {{request()->getHost()}}
    <br class="footer-br"/>
    <a href="https://beian.miit.gov.cn/" target="_blank" ref="nofollow">蜀ICP备16032791号</a>
    @foreach(app("App\Logic\Web\Config\FriendlyService")->getList() as $value)
        <a href="{{$value['site_url']}}" target="_blank" ref="nofollow">{{$value["title"]}}</a>
    @endforeach
</div>

</body>
<script>
    var _hmt = _hmt || [];
    (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?1ce8a30e5d1be9b36c3c252cce920a49";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

</html>
