<?php

use App\Http\Controllers\Web\ArticleController;
use App\Http\Controllers\Web\CollectionController;
use App\Http\Controllers\Web\IndexController;
use Illuminate\Support\Facades\Route;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/5/16
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
Route::get('/', [IndexController::class, "index"]);
// 文章模块
Route::prefix("article")->group( function () {
    Route::get("list/{cate?}", [ArticleController::class, "index"]);
    Route::get("detail/{id?}", [ArticleController::class, "content"]);
});

// 试卷模块
Route::prefix("collection")->group( function () {
    Route::get("list/{id?}", [CollectionController::class, "index"]);
    Route::get("detail/{id?}", [CollectionController::class, "detail"]);
});

