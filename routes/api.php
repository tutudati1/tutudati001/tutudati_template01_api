<?php

declare(strict_types=1);

use App\Http\Controllers\Article\ArticleController;
use App\Http\Controllers\Common\FileUploadController;
use App\Http\Controllers\Config\BannerController;
use App\Http\Controllers\Config\MenuController;
use App\Http\Controllers\Config\NoticeController;
use App\Http\Controllers\Config\UserProfessionController;
use App\Http\Controllers\Document\DocumentController;
use App\Http\Controllers\Document\GroupController;
use App\Http\Controllers\Exam\CategoryController;
use App\Http\Controllers\Exam\CollectionController;
use App\Http\Controllers\Exam\JudeController;
use App\Http\Controllers\Exam\OptionController;
use App\Http\Controllers\Exam\ReadingController;
use App\Http\Controllers\Exam\SubmitController;
use App\Http\Controllers\Exam\UserCollectionController;
use App\Http\Controllers\Member\ConfigController;
use App\Http\Controllers\Message\TemplateController;
use App\Http\Controllers\Resource\ResourceController;
use App\Http\Controllers\User\LoginController;
use App\Http\Controllers\User\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Article\CategoryController as ArticleCategoryController;
use App\Http\Controllers\Resource\CategoryController as ResourceCategoryController;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/19
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
Route::prefix("config")->group(function () {
    Route::get("banner/getList", [BannerController::class, "getList"]);
    Route::get("menu/getList", [MenuController::class, "getList"]);
    Route::get("profession/getList", [UserProfessionController::class, "getList"]);
    Route::get("notice/getList", [NoticeController::class, "getList"]);
});

Route::prefix("exam")->group(function () {
    Route::get("cate/list", [CategoryController::class, "getList"]); // 试卷分类
    Route::get("collection/list", [CollectionController::class, "getList"]); // 试卷列表
    Route::get("collection/rel/list", [CollectionController::class, "getRelList"]); // 试卷相关试题
    Route::get("collection/content", [CollectionController::class, "content"]); // 试卷详情
    Route::get("collection/groupList", [CollectionController::class, "submitGroupList"])->middleware(["weakLoginAuth"]); // 试卷提交用户列表
    Route::get("collection/examList", [CollectionController::class, "getCollectionExamList"])->middleware(["loginAuth"]);// 获取试卷套题
    Route::post("collection/submit", [SubmitController::class, "submit"])->middleware(["loginAuth"]);// 提交试卷答题
    Route::get("collection/submit/info", [SubmitController::class, "getSubmitDetail"])->middleware(["loginAuth"]);// 获取答题结果数据
    Route::get("collection/submit/list", [SubmitController::class, "getList"])->middleware(["loginAuth"]);// 获取答题历史列表
    Route::get("collection/submit/rank/list", [SubmitController::class, "getSubmitRankList"]);// 获取答题历史列表

    Route::get("reading/list", [ReadingController::class, "getList"])->middleware(["loginAuth"]); // 问答试题列表
    Route::get("reading/content", [ReadingController::class, "content"])->middleware(["loginAuth"]); // 问答试题内容
    Route::get("reading/groupList", [ReadingController::class, "submitGroupList"])->middleware(["weakLoginAuth"]); // 问答试题提交用户列表

    Route::get("option/getList", [OptionController::class, "getList"])->middleware(["loginAuth"]); // 选择试题列表

    Route::get("jude/getList", [JudeController::class, "getList"])->middleware(["loginAuth"]);

    Route::post("user/collection/collection", [UserCollectionController::class, "submitCollection"])->middleware(["loginAuth"]); // 试卷收藏
    Route::post("user/collection/reading", [UserCollectionController::class, "submitReadingCollection"])->middleware(["loginAuth"]); // 问答试题收藏
});

Route::prefix("user")->group(function () {
    Route::options("check/login", [UserController::class, "checkLogin"])->middleware(["weakLoginAuth"]);
    Route::post("login", [LoginController::class, "login"]);
    Route::post("update/info", [UserController::class, "updateUserInfo"])->middleware(["loginAuth"]);
    Route::get("avatar/info", [UserController::class, "getUserAvatarInfo"])->middleware(["weakLoginAuth"]);
    Route::get("basic/info", [UserController::class, "getUserBasicInfo"])->middleware(["loginAuth"]);
    Route::post("register", [UserController::class, "register"])->middleware(["loginAuth"]);
    Route::post("account/login", [UserController::class, "login"])->middleware(["loginAuth"]);

    // 会员配置
    Route::get("member/config/getList", [ConfigController::class, "getList"]);
    Route::get("member/config/getContent", [ConfigController::class, "getContent"]);
});

Route::prefix("article")->group(function () {
    Route::get("cate", [ArticleCategoryController::class, "getList"]);
    Route::get("list", [ArticleController::class, "getList"]);
    Route::get("content", [ArticleController::class, "getContent"])->middleware(["weakLoginAuth"]);
    Route::post("collect", [ArticleController::class, "submitCollect"])->middleware(["loginAuth"]);
});

Route::prefix("com")->group(function () {
    Route::post("image/upload", [FileUploadController::class, "imageUpload"])->middleware(["loginAuth"]);
});

Route::prefix("dc")->group(function () {
    Route::get("cate/list", [GroupController::class, "getList"]);
    Route::get("list", [DocumentController::class, "getList"]);
    Route::get("content", [DocumentController::class, "getContent"]);
});

Route::prefix("message")->middleware(["loginAuth"])->group(function () {
    Route::post("template/subscribe", [TemplateController::class, "submitSubscribe"]);
});

Route::prefix("resource")->group(function () {
    Route::get("category/list", [ResourceCategoryController::class, "getList"]);
    Route::get("getList", [ResourceController::class, "getList"]);
    Route::get("getContent", [ResourceController::class, "getContent"]);
    Route::get("getDownloadUrl", [ResourceController::class, "getDownloadUrl"])->middleware(["loginAuth"]);
});
