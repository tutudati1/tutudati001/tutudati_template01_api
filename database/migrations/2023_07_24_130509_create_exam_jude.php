<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamJude extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ex_jude', function (Blueprint $table) {
            $table->bigInteger("id", true, true);
            $table->bigInteger("uid", false, true)->unique("uid");
            $table->bigInteger("collection_uid", false, true)->nullable()->index("collection_uid")->comment("试卷uid");
            $table->bigInteger("category_uid", false, true)->nullable()->index("category_uid")->comment("试卷分类uid");
            $table->string("title", 255);
            $table->json("option");
            $table->decimal("score", 6)->default(0.00);
            $table->tinyInteger("is_correct", false, true)->default(0)->comment("答案序号");
            $table->text("analysis")->nullable();
            $table->integer("sort", false, true)->default(0);
            $table->tinyInteger("is_show", false, true)->default(1);
            $table->dateTime("created_at");
            $table->dateTime("updated_at");
            $table->dateTime("deleted_at")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ex_jude');
    }
}
