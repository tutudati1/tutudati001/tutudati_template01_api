<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleCollect extends Migration
{

    public function up()
    {
        Schema::create('article_collect', function (Blueprint $table) {
            $table->bigInteger("id", true, true);
            $table->bigInteger("uid", false, true)->unique("uid");
            $table->bigInteger("ariticle_uid", false, true)->comment("文章uid");
            $table->bigInteger("user_uid", false, true)->comment("用户uid");
            $table->tinyInteger("is_show", false, true)->default(1);
            $table->dateTime("created_at");
            $table->dateTime("updated_at");
            $table->dateTime("deleted_at")->nullable();
            $table->unique(["user_uid", "ariticle_uid"], "idx_ua");
        });
    }

    public function down()
    {
        Schema::dropIfExists('article_collect');
    }
}
