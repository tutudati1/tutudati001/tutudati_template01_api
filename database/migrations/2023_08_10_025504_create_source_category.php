<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSourceCategory extends Migration
{
    public function up()
    {
        Schema::create('resources_category', function (Blueprint $table) {
            $table->bigInteger("id", true, true);
            $table->bigInteger("uid", false, true)->unique("uid");
            $table->bigInteger("parent_uid", false, true)->nullable()->default(0);
            $table->string("title", 32);
            $table->integer("sort", false, true)->default(0);
            $table->tinyInteger("is_show", false, true)->default(1);
            $table->string("url", 255)->nullable();
            $table->string("path", 1000)->nullable();
            $table->dateTime("created_at");
            $table->dateTime("updated_at");
            $table->dateTime("deleted_at")->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('source_category');
    }
}
