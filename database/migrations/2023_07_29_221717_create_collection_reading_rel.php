<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectionReadingRel extends Migration
{
    public function up()
    {
        Schema::create('ex_collection_reading_rel', function (Blueprint $table) {
            $table->bigInteger("collection_uid", false, true)->index("c_uid");
            $table->bigInteger("exam_uid", false, true)->index("e_uid");
        });
    }

    public function down()
    {
        Schema::dropIfExists('collection_reading_rel');
    }
}
