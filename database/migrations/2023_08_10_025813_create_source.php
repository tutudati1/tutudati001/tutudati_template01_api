<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSource extends Migration
{
    public function up()
    {
        Schema::create('resources', function (Blueprint $table) {
            $table->bigInteger("id", true, true);
            $table->bigInteger("uid", false, true)->unique("uid");
            $table->bigInteger("category_uid", false, true)->index("category_uid");
            $table->string("title", 32);
            $table->integer("sort", false, true)->default(0);
            $table->tinyInteger("is_show", false, true)->default(1);
            $table->string("url", 255)->nullable();
            $table->string("path", 1000)->nullable();
            $table->bigInteger("download_count", false, true)->default(0);
            $table->string("source", 32)->default("兔兔答题");
            $table->string("download_url", 1000)->nullable();
            $table->longText("content")->nullable();
            $table->string("remark")->nullable();
            $table->tinyInteger("is_free", false, true)->default(2);
            $table->decimal("price")->default(0.00);
            $table->dateTime("created_at");
            $table->dateTime("updated_at");
            $table->dateTime("deleted_at")->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('source');
    }
}
