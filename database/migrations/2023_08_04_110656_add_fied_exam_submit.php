<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFiedExamSubmit extends Migration
{
    public function up()
    {
        Schema::table('ex_exam_submit', function (Blueprint $table) {
            $table->integer("exam_count", false, true)->comment("试卷题目数");
            $table->integer("correct_count", false, true)->comment("正确题数");
            $table->integer("error_count", false, true)->comment("错误题数");
        });
    }

    public function down()
    {
        Schema::table('ex_exam_submit', function (Blueprint $table) {
            //
        });
    }
}
