<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamOption extends Migration
{
    public function up()
    {
        Schema::create('ex_option', function (Blueprint $table) {
            $table->bigInteger("id", true, true);
            $table->bigInteger("uid", false, true)->unique("uid");
            $table->bigInteger("collection_uid", false, true)->nullable()->index("collection_uid")->comment("试卷uid");
            $table->bigInteger("category_uid", false, true)->nullable()->index("category_uid")->comment("试卷分类uid");
            $table->string("main_title", 255);
            $table->text("second_title")->nullable();
            $table->json("option");
            $table->decimal("score", 6)->default(0.00);
            $table->json("answer");
            $table->text("analysis")->nullable();
            $table->tinyInteger("type", false, true)->default(1)->comment("1单选2多选");
            $table->integer("sort", false, true)->default(0);
            $table->tinyInteger("is_show", false, true)->default(1);
            $table->dateTime("created_at");
            $table->dateTime("updated_at");
            $table->dateTime("deleted_at")->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ex_option');
    }
}
