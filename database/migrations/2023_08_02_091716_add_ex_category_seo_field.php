<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExCategorySeoField extends Migration
{
    public function up()
    {
        Schema::table('ex_category', function (Blueprint $table) {
            $table->string("seo_title", 255)->comment("描述");
            $table->string("seo_keywords", 255)->comment("描述");
            $table->string("seo_description", 255)->comment("描述");
        });
    }

    public function down()
    {
        Schema::table('ex_category', function (Blueprint $table) {
            //
        });
    }
}
