<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFiedExamSubmitHistory extends Migration
{
    public function up()
    {
        Schema::table('ex_exam_submit_history', function (Blueprint $table) {
            $table->string("origin_answer", 255)->comment("试题原始答案");
            $table->decimal("origin_score", 6)->comment("试题原始积分");
        });
    }

    public function down()
    {
        Schema::table('ex_exam_submit_history', function (Blueprint $table) {
            //
        });
    }
}
