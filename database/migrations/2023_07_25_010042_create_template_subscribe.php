<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTemplateSubscribe extends Migration
{
    public function up()
    {
        Schema::create('template_subscribe', function (Blueprint $table) {
            $table->bigInteger("id", true, true);
            $table->bigInteger("uid", false, true)->unique("uid");
            $table->bigInteger("user_uid", false, true)->index("user_uid")->comment("用户uid");
            $table->bigInteger("template_id", false, true)->index("template_id")->comment("微信模板id");
            $table->string("openid", 255)->index("openid")->comment("用户openid");
            $table->tinyInteger("is_show", false, true)->default(1);
            $table->tinyInteger("is_send", false, true)->default(1)->comment("1待发送2已发送");
            $table->tinyInteger("send_result", false, true)->default(1)->comment("1发送成功2发送失败");
            $table->string("send_msg", 255)->nullable("待发送中")->comment("发送结果描述");
            $table->dateTime("created_at");
            $table->dateTime("updated_at");
            $table->dateTime("deleted_at")->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('template_subscribe');
    }
}
